function validateForm() {
    let x = document.forms["isForm"]["name"].value;
    if (x == "") {
        alert("Tên không được trống");
        return false;
    }
    let y = document.forms["isForm"]["phone"].value;
    if (y == "") {
        alert("Số điện thoại không được trống");
        return false;
    } else {
        var regex = /^[0-9]{10,11}$/;
        if (!regex.test(y)) {
            alert("Số điện thoại không đúng định dạng");
            return false;
        }
    }
    let z = document.forms["isForm"]["email"].value;
    if (z == "") {
        alert("Email không được trống");
        return false;
    } else {
        var emailRegex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        if (!emailRegex.test(z)) {
            alert("Email không đúng định dạng");
            return false;
        }
    }
    
        // Hide the form
    document.forms["isForm"].style.display = "";

    // Show the success message
    document.getElementById("success_message").style.display = "block";

    return false;
    // alert("Đăng ký thành công!");
}
function displayDate() {
    document.getElementById("time").innerHTML = Date();
    setTimeout(displayDate, 1000);
}