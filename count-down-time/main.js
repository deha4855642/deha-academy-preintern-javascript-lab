let h = null; // Giờ
let m = null; // Phút
let s = null;

let timeout = null; // Timeout
let countdownStarted = false; // Initialize with false

function startCountdown() {
    if (!countdownStarted) {
        const confirmation = confirm('Bạn muốn bắt đầu bấm giờ?');
        if (confirmation) {
            countdownStarted = true;

            if (h === null) {
                h = parseInt(document.getElementById('h_key').value);
                m = parseInt(document.getElementById('m_key').value);
                s = parseInt(document.getElementById('s_key').value);
            }

            countdown();
        }
    }
}
function countdown() {
    if (s === -1) {
        m -= 1;
        s = 59;
    }

    if (m === -1) {
        h -= 1;
        m = 59;
    }

    if (h == -1) {
        alert('Hết giờ');
        countdownStarted = false;
        return false;
    }

    document.getElementById('h.run').innerText = h.toString();
    document.getElementById('m.run').innerText = m.toString();
    document.getElementById('s.run').innerText = s.toString();

    timeout = setTimeout(function () {
        s--;
        countdown();
    }, 1000);
}

function stopCountdown() {
    const confirmation = confirm('Bạn muốn dừng bấm giờ?');
    if (confirmation) {
        clearTimeout(timeout);
        countdownStarted = false;
    }
}



function restartCountdown() {
    localStorage.clear();
    h = null;
    m = null;
    s = null;
    clearTimeout(timeout);
    document.getElementById('h.run').innerText = '0';
    document.getElementById('m.run').innerText = '0';
    document.getElementById('s.run').innerText = '0';
}
